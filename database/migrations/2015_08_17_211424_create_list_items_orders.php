<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListItemsOrders extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('list_items_orders', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('checklist_id')->unsigned();
      $table->integer('user_id')->unsigned();
      $table->integer('list_item_id')->unsigned();
      $table->integer('value')->unsigned()->nullable();
      $table->timestamps();
      
      $table->foreign('checklist_id')
        ->references('id')->on('checklists')->onDelete('cascade');
      $table->foreign('user_id')
        ->references('id')->on('users')->onDelete('cascade');
      $table->foreign('list_item_id')
        ->references('id')->on('list_items')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('list_items_orders');
  }

}
