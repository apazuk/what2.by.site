<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListItems extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('list_items', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('checklist_id')->unsigned();
      $table->integer('category_id')->unsigned()->nullable();
      $table->string('name', 200)->index();
      $table->enum('state', ['open','edited','closed'])->default('open');
      $table->timestamps();
      
      $table->foreign('checklist_id')
        ->references('id')->on('checklists')->onDelete('cascade');
      
      $table->foreign('category_id')
        ->references('id')->on('categories')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('list_items');
  }

}
