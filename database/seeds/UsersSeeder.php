<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    DB::table('users')->delete();
    
    for ($index = 0; $index < 10; $index++) {
      $name = 'user' . ($index + 1);
      DB::table('users')->insert([
        'name'     => $name,
        'email'    => $name . '@mail.com',
        'password' => bcrypt('password'),
      ]);
    }
  }

}
