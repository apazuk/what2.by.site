<?php

use Illuminate\Database\Seeder;

class ListsItemsSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('list_items')->delete();

		$users = \App\Models\User::all();

		foreach ($users as $user) {
			$lists = \App\Models\Checklist::whereUserId($user->id)->get();
			$categories = \App\Models\Category::whereUserId($user->id)->get();
			foreach ($lists as $list) {
				foreach ($categories as $category) {
					for ($index = 0; $index < rand(5, 20); $index++) {
						\App\Models\ListItem::create([
							'checklist_id' => $list->id,
							'category_id' => $category->id,
							'name' => 'Item ' . ($index + 1),
							'state' => \App\Models\ListItem::STATE_OPEN
						]);
					}
				}
			}
		}
	}

}
