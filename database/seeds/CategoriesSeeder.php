<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    DB::table('categories')->delete();

    $users = \App\Models\User::all();

    foreach ($users as $user) {
      for ($index = 1; $index <= rand(2, 10); $index++) {
        DB::table('categories')->insert([
          'user_id' => $user->id,
          'name' => 'Category '. $index
        ]);
      }
    }
  }

}
