<?php

use Illuminate\Database\Seeder;

class ListsSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('checklists')->delete();

		$users = \App\Models\User::all();

		foreach ($users as $user) {
			for ($index = 1; $index <= rand(2, 5); $index++) {
				App\Models\Checklist::create([
					'user_id' => $user->id,
					'name' => 'List ' . $index
				]);
			}
		}
	}

}
