<?php

return [
  'login'    => [
    'page_title' => 'Login',
    'title'      => 'Login',
    'sign_in'    => 'Sign in',
  ],
  'register' => [
    'page_title' => 'Register',
    'title'      => 'Register',
    'sign_up'    => 'Sign up',
  ],
];

