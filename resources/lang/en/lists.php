<?php

return [
  'index'  => [
    'page_title'   => 'Lists',
    'title'        => 'Lists',
    'add_list' => 'Add list',
  ],
  'create' => [
    'page_title' => 'Lists :: Create',
    'title'      => 'Create list',
  ],
  'update' => [
    'page_title' => 'Lists :: Update',
    'title'      => 'Update list',
  ],
  'view'  => [
    'page_title'   => 'View',
    'title'        => 'View of :name',
  ],
  'api' => [
    'list_unaccessd' => 'You don\'t have access to this list. Ask the owner to share it first.',
    'list_changed' => 'List ":name" was changed',
    'item_added_to_list' => 'User :user added ":item" to the ":list" list.',
    'item_changed_to_open' => 'User :user changed state of ":item" in the ":list" list to "need".',
  ],
];
