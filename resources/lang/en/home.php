<?php

return [
  'index' => [
    'page_title' => 'Home',
    'logout'     => 'Logout',
  ],
  'lists' => [
    'page_title'                => 'Home',
    'first_list'                => 'Haven\'t created a list yet? Click the "Add list" button above to start creating your own one.',
    'delete_item_prompt'        => 'Are you sure to delete this item?',
    'people'                    => 'People',
    'emails_separated_by_comma' => 'Input emails separated by comma.'
  ],
];

