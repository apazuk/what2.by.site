<?php

return [
  'login' => [
    'email' => 'Email',
    'password' => 'Password',
    'sign_in' => 'Sign in',
    'sign_up' => 'Sign up',
  ],
  'modals' => [
    'delete' => [
      'title' => 'Item deleting'
    ],
  ],
];

