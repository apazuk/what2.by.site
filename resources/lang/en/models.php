<?php

return [
  'user'       => [
    'name'             => 'Name',
    'email'            => 'Email',
    'password'         => 'Password',
    'confirm_password' => 'Confirm password',
  ],
  'categories' => [
    'user_id' => 'User',
    'name'    => 'Name',
  ],
  'lists'      => [
    'user_id' => 'User',
    'name'    => 'Name',
  ],
  'list_items' => [
    'checklist_id' => 'List',
    'name'         => 'Name',
    'state'        => 'State',
  ],
];
