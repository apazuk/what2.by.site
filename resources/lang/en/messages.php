<?php

return array(
  'success' => array(
    'added'               => ':name has been added successfully',
    'updated'             => ':name has been updated successfully',
    'deleted'             => ':name has been deleted successfully',
    'shared'              => ':name has been shared successfully',
    'account_was_created' => 'Account was created successfully and email with the activation code has been sent to you.'
  ),
  'info'    => array(
    'account_was_activated' => 'Looks like this account has already been activated.',
    'no_items_found' => '&lt;No :name found.&gt;',
  ),
  'warning' => array(
  ),
  'error'   => array(
    '404_header'                 => 'Page not found.',
    '404_text'                   => 'Please check if this URL is correct.',
    'validation_header'          => 'Please, fix the following errors and submit again.',
    'something_wrong'            => 'Something went wrong. Try again later.',
    'check_errors'               => 'Check the following errors and try again.',
    'email_required'             => 'Please input your e-mail.',
    'password_required'          => 'Please input your password.',
    'email_or_password_required' => 'Please provide your email address and password.',
    'incorrect_email_password'   => 'The provided email and password was not found. Please try again.',
    'incorrect_password'         => 'The provided password is not correct. Please try again.',
    'record_not_found'           => ':name not found',
    'session_expired'            => 'Your session has expired. Please refresh the page and try again.',
    'activation_code_not_found'  => 'Activation code is not correct.',
    'account_is_not_activated'   => 'Activate your account before login.',
    'account_is_blocked'         => 'Your account is blocked.',
  ),
  'prompt'  => array(
    'delete' => 'Are you sure to delete this :name?'
  )
);
