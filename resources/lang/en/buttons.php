<?php

return [
  'create'   => 'Create',
  'update'   => 'Update',
  'ok'       => 'OK',
  'cancel'   => 'Cancel',
  'save'     => 'Save',
  'delete'   => 'Delete',
  'edit'     => 'Edit',
  'register' => 'Register',
  'add'      => 'Add',
  'share'    => 'Share',
];
