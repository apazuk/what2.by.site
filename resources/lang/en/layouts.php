<?php

return [
  'main' => [
    'logout' => 'Logout',
    'menu'   => [
      'lists'      => 'Lists',
      'add_list'   => 'Add List',
      'categories' => 'Categories',
    ]
  ]
];

