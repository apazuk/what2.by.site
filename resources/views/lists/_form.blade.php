@include('messages._validation')

<form action="" class="form-horizontal" method="POST">
  {!! csrf_field() !!}
  <div class="form-group @if($errors->has('name')) has-error @endif">
    <label for="name" class="col-sm-1 control-label">{{trans('models.lists.name')}}</label>
    <div class="col-sm-11">
      <input type="text" name="name" class="form-control" id="name" value="{{$list->name}}">
    </div>
  </div>
  <div class="form-group text-right">
    <div class="col-sm-12">
      <a href="{{url('lists')}}" class="btn btn-default" role="button">{{trans('buttons.cancel')}}</a>
      @if ($list->id)
      <button type="submit" class="btn btn-success">{{trans('buttons.update')}}</button>
      @else
      <button type="submit" class="btn btn-success">{{trans('buttons.create')}}</button>
      @endif
    </div>
  </div>
</form>

