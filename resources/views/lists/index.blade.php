@extends('layouts.main', array('page_title' => trans('lists.index.page_title')))

@section('content')
<div class="row">
  <div class="col-xs-6">
    <h1 class="page-title">{{trans('lists.index.title')}}</h1>
  </div>
  <div class="col-xs-6  text-right">
    <a href="{{url('lists/create')}}" class="btn btn-success" role="button"><span class="fa fa-plus" aria-hidden="true"></span>&nbsp;{{trans('lists.index.add_list')}}</a>
  </div>
</div>

@include('messages._custom')

@include('lists._table')

@endsection


@section('scripts')
  <script language="javascript">
    $(document).ready(function () {
      App.hideAlerts();
    });
  </script>
@endsection

