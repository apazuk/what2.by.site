@extends('layouts.main', array('page_title' => trans('lists.create.page_title')))

@section('content')
<div class="row">
  <div class="col-xs">
    <h1 class="page-title">{{trans('lists.create.title')}}</h1>
  </div>
</div>

@include('lists._form')

@endsection