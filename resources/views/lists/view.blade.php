@extends('layouts.main', array('page_title' => trans('lists.view.page_title')))

@section('content')

@include('messages._custom')

<div class="col-xs-12">
  <div id="list-{{$list->id}}" data-id="{{$list->id}}" class="panel panel-default checklist">
    @include('home.lists._list_heading', ['list' => $list])
    @include('home.lists._list_body', ['items' => $list->items])
  </div>
</div>

@include('home.lists.modals._create')
@include('home.lists.modals._update')
@include('home.lists.modals._delete')
@include('home.lists.modals._share')

@endsection

@section('plugins')
  <script src="{{asset('assets/js/lists.js')}}"></script>
@endsection

@section('scripts')
  <script language="javascript">
$(document).ready(function () {
  Lists.init({
  });
});
  </script>
@endsection