@include('modals._delete', ['name' => 'list'])

<table class="table table-striped">
  <thead>
    <tr>
      <th>
        {{(trans('models.lists.name'))}}
      </th>
      <th class="actions"></th>
    </tr>
  </thead>
  <tbody>
    @forelse($lists as $list)
    <tr>
      <td>
        {{$list->name}}
      </td>
      <td class="actions">
        <a href="{{url('lists/update', ['id' => $list->id])}}" class="btn btn-success btn-xs edit" title="{{trans('buttons.edit')}}"><span class="fa fa-pencil"></span></a>
        <button data-href="{{url('lists/delete', ['id' => $list->id])}}" type="button" class="btn btn-danger btn-xs delete" data-toggle="modal" data-target="#modal-delete" title="{{trans('buttons.delete')}}" ><span class="fa fa-remove"></span></button>
      </td>
    </tr>
    @empty
    <tr>
      <td rowspan="2" class="text-center text-muted">
        <span>&lt;No lists found&gt;</span>
      </td>
    </tr>
    @endforelse
  </tbody>
</table>

