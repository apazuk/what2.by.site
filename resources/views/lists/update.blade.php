@extends('layouts.main', array('page_title' => trans('lists.update.page_title')))

@section('content')
<div class="row">
  <div class="col-xs">
    <h1 class="page-title">{{trans('lists.update.title')}}</h1>
  </div>
</div>

@include('lists._form')

@endsection