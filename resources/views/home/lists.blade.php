@extends('layouts.main', array('page_title' => trans('home.lists.page_title')))

@section('content')

@include('messages._blank')

<div id="lists" class="container-fluid">
  @if ($listsMy->isEmpty() && $listsShared->isEmpty())
    @include('home.lists._first')
  @endif
  
  @foreach($listsMy as $list)
    @include('home.lists._list', ['list' => $list])
  @endforeach
  
  @foreach($listsShared as $list)
    <?php $list->id = $list->checklist_id; ?>
    @include('home.lists._list', ['list' => $list])
  @endforeach
</div>

@include('home.lists.modals._create')
@include('home.lists.modals._update')
@include('home.lists.modals._delete')
@include('home.lists.modals._share')

@endsection

@section('plugins')
  <script src="{{asset('assets/js/lists.js')}}"></script>
@endsection

@section('scripts')
  <script language="javascript">
$(document).ready(function () {
  Lists.init({
  });
});
  </script>
@endsection
