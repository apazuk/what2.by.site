          <div class="btn-group">
            <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i></button>
            <ul class="dropdown-menu">
              <li>      
                <a href="#modal-share" class="btn-share" role="button" data-href="{{url('api/lists/share', ['id' => $list->id])}}" data-class="list" data-id="{{$list->id}}" data-toggle="modal">
                  <i class="fa fa-share-alt text-success"></i>
                </a>
              </li>
              <li role="separator" class="divider"></li>
              <li>      
                <a href="#modal-update" class="btn-edit" role="button" data-href="{{url('api/lists/update', ['id' => $list->id])}}" data-class="list" data-id="{{$list->id}}" data-toggle="modal">
                  <span class="fa fa-pencil text-primary"></span>
                </a>
              </li>
              <li role="separator" class="divider"></li>
              <li>      
                <a href="#modal-delete" class="btn-delete" role="button" data-href="{{url('api/lists/delete', ['id' => $list->id])}}" data-toggle="modal">
                  <span class="fa fa-remove text-danger"></span>
                </a>
              </li>
            </ul>
          </div>