<div id="modal-update" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="name" class="col-sm-2 control-label">{{trans('models.list_items.name')}}</label>
            <div class="col-sm-10">
              <input type="text" name="name" class="form-control" autofocus>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <form action="" method="POST">
          <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('buttons.cancel')}}</button>
          <button type="submit" class="btn btn-primary">{{trans('buttons.save')}}</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->