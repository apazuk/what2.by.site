<div id="modal-share" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="people" class="col-sm-2 control-label">{{trans('home.lists.people')}}</label>
            <div class="col-sm-10">
              <input type="text" name="people" class="form-control" autofocus value="">
              <span class="help-block">{{trans('home.lists.emails_separated_by_comma')}}</span>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <form action="" method="POST">
          <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('buttons.cancel')}}</button>
          <button type="submit" class="btn btn-primary">{{trans('buttons.share')}}</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->