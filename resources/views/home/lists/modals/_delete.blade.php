<div id="modal-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p>{{trans('home.lists.delete_item_prompt')}}</p>
      </div>
      <div class="modal-footer">
        <form action="" method="POST">
          <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('buttons.cancel')}}</button>
          <button type="submit" class="btn btn-danger">{{trans('buttons.delete')}}</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->