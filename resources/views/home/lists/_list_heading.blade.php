    <div class="panel-heading checklist-heading">
      <div class="row">
        <div class="col-xs-7">
          <h3 class="panel-title name">{{$list->name}}</h3>
        </div>
        <div class="col-xs-3 text-right no-left-padding dashboard">
          @if(count($list->sharedTo) > 0)
          <span class="shares"><i class="fa fa-users"></i><i class="value">{{count($list->sharedTo)}}</i></span>
          @endif
        </div>
        <div class="col-xs-2 text-right no-left-padding actions">
          @include('home.lists._list_menu', ['list' => $list])
        </div>      
      </div>

    </div>