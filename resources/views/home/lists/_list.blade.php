<div class="col-xs col-sm col-md-6 col-lg-4">
  <div id="list-{{$list->id}}" data-id="{{$list->id}}" class="panel panel-default checklist">
    @include('home.lists._list_heading', ['list' => $list])
    @include('home.lists._list_body', ['items' => $list->items])
  </div>
</div>

