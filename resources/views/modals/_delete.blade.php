<div id="modal-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <b>{{trans('forms.modals.delete.title')}}</b>
      </div>
      <div class="modal-body">
        <p>{{trans('messages.prompt.delete', array('name' => $name))}}</p>
      </div>
      <div class="modal-footer">
        <form action="" method="POST">
        {!! csrf_field() !!}
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('buttons.cancel')}}</button>
        <button type="submit" class="btn btn-danger">{{trans('buttons.delete')}}</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->