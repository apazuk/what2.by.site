@extends('layouts.main', array('page_title' => trans('auth.register.page_title')))

@section('content')
<div class="well">
  <div class="alert alert-success" role="alert">{{$message}}</div>
</div>
@endsection