@extends('layouts.main', array('page_title' => trans('auth.login.page_title')))

@section('content')
<div class="col-sm-6 col-sm-offset-3">
  <h1 class="page-title text-center">{{trans('auth.login.title')}}</h1>
  
  @include('messages._validation')
  
  <form class="form-horizontal" method="POST" action="{{url('login')}}">
    {!! csrf_field() !!}
    <div class="form-group @if($errors->has('email')) has-error @endif">
      <label for="email" class="col-sm-3 col-md-2 control-label">{{trans('models.user.email')}}</label>
      <div class="col-sm-9 col-md-10">
        <input type="text" name="email" class="form-control" id="email" value="{{ old('email') }}">
      </div>
    </div>
    <div class="form-group @if($errors->has('password')) has-error @endif">
      <label for="password" class="col-sm-3 col-md-2 control-label">{{trans('models.user.password')}}</label>
      <div class="col-sm-9 col-md-10">
        <input type="password" name="password" class="form-control" id="password">
      </div>
    </div>

    <div class="form-group text-right">
      <div class="col-sm-12">
        <button type="submit" class="btn btn-success">{{trans('auth.login.sign_in')}}</button>
      </div>
    </div>
  </form>
</div>


@endsection


@section('scripts')
<script language="javascript">
  $(document).ready(function () {
    App.hideAlerts();
  });
</script>
@endsection

