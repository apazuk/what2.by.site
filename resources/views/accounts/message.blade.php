@extends('layouts.main', array('page_title' => trans('accounts.message.page_title')))

@section('content')
<div class="well">
  <div class="alert alert-{{$type}}" role="alert">{{$message}} <a href="{{url('/')}}" class="alert-link">{{trans('accounts.message.go_to_home_page')}}</a></div>
</div>
@endsection