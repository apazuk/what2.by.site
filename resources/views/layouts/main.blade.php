<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">

    <title>{{config('app.project_name')}} :: {{{$page_title}}}</title>

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    @if (env('APP_ENV') == 'local')

    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
    @else
    <!--    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    @endif

    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('styles')
  </head>

  <body>

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{url()}}">{{config('app.project_name')}}</a>
        </div>

        @if (!Auth::check())
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" action="{{url('login')}}" method="POST">
            {!! csrf_field() !!}
            <div class="form-group">
              <input type="text" name="email" value="{{old('email')}}" tabindex="1" placeholder="{{trans('forms.login.email')}}" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" name="password" tabindex="2" placeholder="{{trans('forms.login.password')}}" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">{{trans('forms.login.sign_in')}}</button>
            <a href="{{url('register')}}" class="btn btn-primary">{{trans('forms.login.sign_up')}}</a>
          </form>
        </div><!--/.navbar-collapse -->   
        @else
        <div id="navbar" class="navbar-collapse collapse">
          <!--<p class="navbar-text">{{date('H:i:s')}}</p>-->
          @include('layouts._main_menu')
        </div>
        @endif

      </div>
    </nav>

    <div class="container">
      <div class="content">
        @yield('content')
      </div>
    </div>

    @if (env('APP_ENV') == 'local')
    <script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.js')}}"></script>
    @else
    <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    @endif
    <script src="{{asset('assets/js/app.js')}}"></script>

    <script language="javascript">
$(document).ready(function () {
  App.init({
    basePath: '{{url("/")}}',
    apiPath: '{{url("api")}}',
    apiToken: '{{csrf_token()}}'
  });
});
    </script>

    @yield('plugins') 
    @yield('scripts')
  </body>
</html>
