<ul class="nav navbar-nav navbar-right">
  <li><a href="#modal-create" data-href="{{url('api/lists/create')}}" role="button" data-toggle="modal"><span class="fa fa-plus-circle"></span>&nbsp;{{trans('layouts.main.menu.add_list')}}</a></li>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user"></span>&nbsp;{{Auth::User()->name}}&nbsp;<span class="caret"></span></a>
    <ul class="dropdown-menu">
      <!--<li><a href="{{url('lists')}}">{{trans('layouts.main.menu.lists')}}</a>-->
      <!--<li role="separator" class="divider">-->
      <li><a href="{{url('logout')}}">{{trans('layouts.main.logout')}}</a>
    </ul>
  </li>
</ul>