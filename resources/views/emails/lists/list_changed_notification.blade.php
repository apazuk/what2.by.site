<table style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#f5f8fa;margin:0;padding:0;width:100%" width="100%" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box" align="center">
                <table style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;margin:0;padding:0;width:100%" width="100%" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;padding:15px 0;text-align:center">
								<a href="{{url()}}" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#bbbfc3;font-size:19px;font-weight:bold;text-decoration:none" target="_blank" >What To Buy
								</a>
							</td>
						</tr>
						<tr>
							<td cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#ffffff;border-bottom:1px solid #edeff2;border-top:1px solid #edeff2;margin:0;padding:0;width:100%" width="100%">
								<table style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#ffffff;margin:0 0;padding:0;width:100%" width="100%" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;padding:5px">

<p style="font-weight: bold;">Recent changes to the <a href="{{url('lists/view/'. $list->id)}}" target="_blank">{{$list->name}}</a> list:</p>

<ul style="list-style:none;margin:0;padding:0 0 10px 20px;">
	@foreach($items as $item)
	<?php
	switch ($item->state):
		case \App\Models\ListItem::STATE_OPEN:
			if (strtotime($item->updated_at) > strtotime('- 10 minutes')):
				?>
				<li style="color: #27ae60;">{{$item->name}}</li>
			<?php else: ?>
				<li style="color: #34495e;">{{$item->name}}</li>
			<?php
			endif;
			break;
		case \App\Models\ListItem::STATE_CLOSED:
			if (strtotime($item->updated_at) > strtotime('- 10 minutes')):
				?>
				<li style="color: #95a5a6;text-decoration:line-through;">{{$item->name}}</li>
					<?php
				endif;
				break;
		endswitch;
		?>
	@endforeach
</ul>
												
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
        </tr>
	</tbody>
</table>