<!-- validation errors -->
@if ($errors->has())
<div class="alert alert-danger">
  <!--<b>{{trans('messages.error.validation_header')}}</b>-->
  <ul class="no-bullet">
  @foreach ($errors->all('<li>:message</li>') as $message)
    {!!$message!!}
  @endforeach
  </ul>
</div>
@endif

