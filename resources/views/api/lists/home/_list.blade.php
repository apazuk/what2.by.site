<div class="col-xs col-sm col-md-6 col-lg-4">
  <div id="list-{{$list->id}}" data-id="{{$list->id}}" class="panel panel-default checklist">
    <div class="panel-heading checklist-heading">
      <div class="row">
        <div class="col-xs-7">
          <h3 class="panel-title name">{{$list->name}}</h3>
        </div>
        <div class="col-xs-3 text-right no-left-padding dashboard">
          @if(count($list->sharedTo) > 0)
          <span class="shares"><i class="fa fa-users"></i><i class="value">{{count($list->sharedTo)}}</i></span>
          @endif
        </div>
        <div class="col-xs-2 text-right no-left-padding actions">
          @include('api.lists.home._list_menu', ['list' => $list])
        </div>      
      </div>
    </div>

    <div class="panel-body checklist-body">
      @include('api.lists.items._content', ['list' => $list, 'items' => $list->items])
    </div>

  </div>
</div>