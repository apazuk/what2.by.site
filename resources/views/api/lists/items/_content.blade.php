      <ul class="list-group items">
        <li class="list-group-item">
          <div class="row text-center">
            <a href="#modal-create" class="btn-block btn-default btn-add" data-href="{{url('api/lists/items/create', ['id' => $list->id])}}" role="button" data-toggle="modal">
              <span class="fa fa-plus-circle text-success"></span>&nbsp;Add Item
            </a>     
          </div>
        </li>
        @foreach($list->items as $item)
        @include('api.lists.items._item', ['item' => $item])
        @endforeach
      </ul>