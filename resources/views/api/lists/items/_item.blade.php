<li class="list-group-item" id="list-item-{{$item->id}}">
  <div class="row">
    <div class="col-xs-9 list-item name">@if($item->state == \App\Models\ListItem::STATE_CLOSED)<strike class="text-muted">{{$item->name}}</strike>@else{{$item->name}}@endif</div> 

    <div class="col-xs-3 pull-right text-right list-item action">
      <a href="{{url('api/lists/items/check', ['id' => $item->id])}}" class="btn btn-xs btn-default btn-check">
        @if($item->state == \App\Models\ListItem::STATE_CLOSED)
        <span class="fa fa-check-square-o"></span>
        @else
        <span class="fa fa-square-o"></span>
        @endif
      </a>
      <div class="btn-group">
        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></button>
        <ul class="dropdown-menu">
          <li>      
            <a href="#modal-update" class="btn-edit" role="button" data-href="{{url('api/lists/items/update', ['id' => $item->id])}}" data-class="list.item" data-id="{{$item->id}}" data-toggle="modal">
              <span class="fa fa-pencil text-primary"></span>
            </a>
          </li>
          <li role="separator" class="divider"></li>
          <li>      
            <a href="#modal-delete" class="btn-delete" role="button" data-href="{{url('api/lists/items/delete', ['id' => $item->id])}}" data-toggle="modal">
              <span class="fa fa-remove text-danger"></span>
            </a>
          </li>
        </ul>
      </div>
    </div> 
  </div>
</li>

