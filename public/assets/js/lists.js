var Lists = function () {
  var _self = this;
  var _settings = [];
  var _listsRefreshingCount = 0;
  var _STATE_OPEN = 'open';
  var _STATE_EDITED = 'edited';
  var _STATE_CLOSED = 'closed';
  
  function moveItemUp(id) {
    var item = $('#list-item-' + id);
    var list = $(item).closest('.items');
    var first = $('li.list-group-item', list).eq(1);

    callback = function () {
      item.insertBefore(first);
    };
    item.slideUp(500, callback).slideDown(500);
  }

  function moveItemDown(id) {
    var item = $('#list-item-' + id);
    var list = $(item).closest('.items');
    var last = $('li.list-group-item:last-child', list);

    callback = function () {
      item.insertAfter(last);
    };
    item.slideUp(500, callback).slideDown(500);
  }
  
  function onCreateModalShow() {
    $('#modal-create').on('show.bs.modal', function (e) {
      $('.modal-body form input[name="name"]', '#modal-create').val('').focus();

      $('.modal-body form, .modal-footer form', '#modal-create').attr('action', $(e.relatedTarget).attr('data-href'));
    });
  }

  function onItemCheckClick() {
    $('.checklist').on('click', '.btn-check', function (e) {
      e.preventDefault();

      $.post($(this).attr('href'),
        {
          _token: App.getApiToken(),
          render: 'html'
        },
      'json')
        .done(function (data) {
          $('#list-item-' + data.id).replaceWith(data.item);

          switch (data.state) {
            case _STATE_CLOSED:
              moveItemDown(data.id);
              break;
            case _STATE_OPEN:
              moveItemUp(data.id);
              break;
          }
        })
        .always(function () {
          App.hideAlerts();
        });

    });
  }

  function onItemAddClick() {
    $('.list-group.items').on('click', '.btn-add', function(e){
      e.preventDefault();
      
      $('.modal-body form input[name="checklist_id"]', '#modal-create').val($(e.target).closest('.checklist').attr('data-id'));
    });
  }

  function onItemEditClick() {
    $('#modal-update').on('show.bs.modal', function (e) {
      var id = $(e.relatedTarget).attr('data-id');
      var c = $(e.relatedTarget).attr('data-class');
      var val = '';
      
      switch (c) {
        case 'list':
          val = $('.checklist-heading .name', '#list-' + id).text();
          break;
        case 'list.item':
          val = $('.list-item.name', '#list-item-' + id).text().trim();
          break;
      }

      $('.modal-body form input[name="name"]', '#modal-update').val(val).focus();

      $('.modal-body form', '#modal-update').attr('action', $(e.relatedTarget).attr('data-href'));
      $('.modal-footer form', '#modal-update').attr('action', $(e.relatedTarget).attr('data-href'));
    });
  }

  function onItemDeleteClick() {
    $('#modal-delete').on('show.bs.modal', function (e) {
      $('.modal-footer form', '#modal-delete').attr('action', $(e.relatedTarget).attr('data-href'));
    });
  }

  function onItemCreateSubmit() {
    $('#modal-create form').submit(function (e) {
      e.preventDefault();

      $('#modal-create').modal('hide');

      $.post($(this).attr('action'),
        {
          _token: App.getApiToken(),
          render: 'html',
          checklist_id: $('.modal-body form input[name="checklist_id"]', '#modal-create').val(),
          name: $('.modal-body form input[name="name"]', '#modal-create').val()
        },
      'json')
        .done(function (data) {
          switch (data.class) {
            case 'list':
              $('#lists').append(data.result);
              break;
            case 'list.item':
              var list = $('.items', '#list-' + data.checklist_id);
              var first = $('li.list-group-item', list).eq(1);

              if (first.length === 0)
                $(list).append(data.item);
              else
                $(data.item).insertBefore(first);
              break;
          }
        })
        .always(function () {
          App.hideAlerts();
        });
    });
  }

  function onItemUpdateSubmit() {
    $('#modal-update form').submit(function (e) {
      e.preventDefault();

      $('#modal-update').modal('hide');

      $.post($(this).attr('action'),
        {
          _token: App.getApiToken(),
          render: 'html',
          name: $('.modal-body form input[name="name"]', '#modal-update').val()
        },
      'json')
        .done(function (data) {
          switch (data.class) {
            case 'list':
              $('.checklist-heading .name', '#list-' + data.id).text(data.name);
              break;
            case 'list.item':
              $('#list-item-' + data.id).replaceWith(data.item);
              break;
          }
        })
        .always(function () {
          App.hideAlerts();
        });
    });
  }

  function onItemDeleteSubmit() {
    $('#modal-delete form').submit(function (e) {
      e.preventDefault();

      $('#modal-delete').modal('hide');

      $.post($(this).attr('action'),
        {
          _token: App.getApiToken(),
          render: 'html'
        },
      'json')
        .done(function (data) {
          switch (data.class) {
            case 'list':
              $('#list-' + data.id).parent().remove();
              break;
            case 'list.item':
              $('#list-item-' + data.id).remove();
              break;
          }
        })
        .always(function () {
          App.hideAlerts();
        });
    });
  }

  function onListShareClick() {
    $('#modal-share').on('show.bs.modal', function (e) {
      console.log($(e.relatedTarget).attr('data-href'));
      $('.modal-footer form', '#modal-share').attr('action', $(e.relatedTarget).attr('data-href'));
    });
  }

  function onListShareSubmit() {
    $('#modal-share form').submit(function (e) {
      e.preventDefault();

      $('#modal-share').modal('hide');

      $.post($(this).attr('action'),
        {
          _token: App.getApiToken(),
          people: $('.modal-body form input[name="people"]', '#modal-share').val()
        },
      'json')
        .done(function (data) {
          switch (data.class) {
            case 'list':
              $('.checklist .checklist-heading .name', '#list-' + data.id).text(data.name);
              break;
            case 'list.item':
              $('#list-item-' + data.id).replaceWith(data.html);
              break;
          }
        })
        .always(function () {
          App.hideAlerts();
        });
    });
  }

  function setGlobalEvents() {
    onCreateModalShow();
    
    onItemCheckClick();
    onItemEditClick();
    onItemDeleteClick();
    onItemAddClick();

    onItemUpdateSubmit();
    onItemDeleteSubmit();
    onItemCreateSubmit();

    onListShareClick();
    onListShareSubmit();
  }

  function onRefreshListBegin() {
    _listsRefreshingCount++;
  }

  function onRefreshListEnd() {
    _listsRefreshingCount--;

    if (_listsRefreshingCount === 0)
      setGlobalEvents();
  }

  function refreshList(id) {
    onRefreshListBegin();

    $.get(App.getApiPath() + '/lists/view/' + id,
      {
        render: 'html'
      },
    'json')
      .done(function (data) {
        $('#list-' + data.id).parent().replaceWith(data.result);
      })
      .always(function () {
        onRefreshListEnd();
        App.hideAlerts();
      });
  }

  function runListsAutorefresh() {
    setInterval(function () {
      $('.checklist').each(function (index, em) {
        refreshList($(em).attr('data-id'));
      });
    }, 5 * 60 * 1000);
  }

  return {
    //main function to initiate class
    init: function (settings) {
      _self._settings = settings;

      setGlobalEvents();

      runListsAutorefresh();
    }
  };
}();