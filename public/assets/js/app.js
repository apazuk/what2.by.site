var App = function () {
  var _self = this;
  var BASE_PATH = '';
  var API_PATH = '';
  var API_TOKEN = '';

  //  makes fake object for IE
  function makeFakeConsole() {
    if (typeof console === 'undefined')
      console = {
        log: function () {
        }
      };
  }
  
  function onAjaxError() {
    $(document).ajaxError(
      function (event, jqxhr, settings, thrownError) {
        console.log('event', event);
        console.log('jqxhr', jqxhr);
        console.log('settings', settings);
        console.log('thrownError', thrownError);

        switch (jqxhr.status) {
          case 401:
            location.reload();
            break;
          default:
            $('.alert.alert-danger')
              .text(jqxhr.statusText)
              .removeClass('hide')
              .show();
            break;
        }
      });
  }

  function onTableDeleteClick() {
    $('#modal-delete').on('show.bs.modal', function (e) {
      $('.modal-footer form', '#modal-delete').attr('action', $(e.relatedTarget).attr('data-href'));
    });
  }

  function setGlobalEvents() {
    onAjaxError();
    
    onTableDeleteClick();
  }

  return {
    //main function to initiate class
    init: function (settings) {

      if (settings.basePath !== '')
        _self.BASE_PATH = settings.basePath;

      if (settings.apiPath !== '')
        _self.API_PATH = settings.apiPath;

      if (settings.apiToken !== '')
        _self.API_TOKEN = settings.apiToken;

      makeFakeConsole();

      setGlobalEvents();
    },
    hideAlerts: function () {
      setTimeout(function () {
        $('.alert.alert-success').hide(1000);
      }, 3000);
      setTimeout(function () {
        $('.alert.alert-danger').hide(1000);
      }, 5000);
    },
    getBasePath: function () {
      return _self.BASE_PATH;
    },
    getApiPath: function () {
      return _self.API_PATH;
    },
    getApiToken: function () {
      return _self.API_TOKEN;
    }
  };
}();