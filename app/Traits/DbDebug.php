<?php

namespace App\Traits;

/**
 * Description of DbDebug
 *
 * @author Alex
 */
trait DbDebug
{

	public function enableLoggin()
	{
		\DB::enableQueryLog();
	}

	public function getLastQuery()
	{
		$_log = \DB::getQueryLog();

		return end($_log);
	}

	public function dumpLastQuery()
	{
		$log = $this->getLastQuery();

		$query = $log['query'];
		$bindings = $log['bindings'];

		if (is_array($bindings)) {
			$from = '/' . preg_quote('?', '/') . '/';
			foreach ($bindings as $value)
				$query = preg_replace($from, "'{$value}'", $query, 1);
		}

		echo $query;
	}

}
