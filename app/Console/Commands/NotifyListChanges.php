<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Checklist;
use App\Jobs\SendListChangesNotificationEmail;

class NotifyListChanges extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'lists:notify';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Selects the recently changed lists and puts notifications into a queue.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$items = Checklist::distinct()
			->select('checklists.*')
			->join('list_items as li', function ($join) {
				$join->on('checklists.id', '=', 'li.checklist_id');
			})
			->whereBetween('li.updated_at', [
				date('Y-m-d H:i:s', strtotime('- 10 minutes')),
				date('Y-m-d H:i:s', strtotime('- 5 minutes'))])
			->get();
			
		$this->info(count($items) . ' lists found');

		foreach ($items as $item) {
			$this->info("Put notification of {$item->id} into the mail queue");

			$job = (new SendListChangesNotificationEmail($item))->onQueue('mail');

			app('Illuminate\Bus\Dispatcher')->dispatch($job);
		}
	}

}
