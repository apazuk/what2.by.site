<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $activation_token
 * @property string $status Available values: 'new','activated','blocked'
 * @property timestamp $last_visit
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

  const STATUS_NEW = 'new';
  const STATUS_ACTIVATED = 'activated';
  const STATUS_BLOCKED = 'blocked';

  use Authenticatable,
      CanResetPassword;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'users';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'email',
    'password',
    'activation_token',
    'status',
    'last_visit'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['password',
    'remember_token',
    'activation_token',
    'last_visit',
    'created_at',
    'updated_at'];

}
