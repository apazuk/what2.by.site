<?php

namespace App\Models;

/**
 * @property integer $user_id
 * @property string $name
 */
class Category extends AbstractModel {

  protected $fillable = [
    'user_id',
    'name',
  ];
  protected $hidden = [
    'created_at',
    'updated_at'
  ];
  protected $validationRules = [
    'name' => 'required|max:100',
  ];

  public function scopeMy($query) {
    return $query->whereUserId(\Auth::User()->id);
  }

}
