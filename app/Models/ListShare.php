<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListShare extends Model {

  protected $fillable = [
    'checklist_id',
    'user_id',
  ];
  protected $hidden = [
    'created_at',
    'updated_at'
  ];
  
  public function user() {
    return $this->belongsTo('App\Models\User');
  }

}
