<?php

namespace App\Models;

class Checklist extends AbstractModel {

  protected $fillable = [
    'user_id',
    'name',
  ];
  protected $hidden = [
    'user_id',
    'sharedTo',
    'created_at',
    'updated_at'
  ];
  protected $validationRules = [
    'name' => 'required|max:100',
  ];

  public function items() {
    return $this->hasMany('App\Models\ListItem')
		->orderBy('state')
		->orderBy('name');
  }
  
  public function user() {
    return $this->belongsTo('App\Models\User');
  }

  public function sharedTo() {
    return $this->hasMany('App\Models\ListShare');
  }

  public function scopeShared($query) {
    return $query->join('list_shares', function($join) {
      $join->on('checklists.id', '=', 'list_shares.checklist_id')
        ->where('list_shares.user_id', '=', \Auth::User()->id);
    });
  }

  public function scopeMy($query) {
    return $query->whereUserId(\Auth::User()->id);
  }
  
  public function iHaveAccess() {
    $shares = $this->sharedTo;

    $haveAccess = $this->user_id == \Auth::User()->id;
//    var_dump ([1 => [$list->user_id. ' == '. \Auth::User()->id, $haveAccess ? 'yes' : 'no']]);
    foreach ($shares as $item) {
      if ($item->user_id == \Auth::User()->id)
        $haveAccess |= true;
//      var_dump ([2 => [$item->user_id. ' == '. \Auth::User()->id, $haveAccess ? 'yes' : 'no']]);
    }
    
    return $haveAccess;
  }

}
