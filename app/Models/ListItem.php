<?php

namespace App\Models;

class ListItem extends AbstractModel {

  const STATE_OPEN = 'open';
  const STATE_EDITED = 'edited';
  const STATE_CLOSED = 'closed';

  protected $fillable = [
    'checklist_id',
    'category_id',
    'name',
    'state'
  ];
  protected $hidden = [
    'created_at',
    'updated_at'
  ];
  protected $validationRules = array(
    'name'  => 'required|max:200',
    'state' => 'required|in:open,edited,closed',
  );
  
  public function checklist() {
    return $this->belongsTo('App\Models\Checklist');
  }

  static public function getStates() {
    return array(
      'open',
      'edited',
      'closed'
    );
  }

  public static function getLabelByState($state) {
    switch ($state) {
      case self::STATE_OPEN:
        return 'default';
      case self::STATE_OPEN:
        return 'primary';
      case self::STATE_CLOSED:
        return 'success';
    }
  }

}
