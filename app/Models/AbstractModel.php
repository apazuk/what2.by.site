<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of AbstractModel
 *
 * @author alex
 */
class AbstractModel extends Model {

  protected $validationRules = null;
  protected $validationMessages = array();
  protected $validationErrors = null;

  public static function boot() {
    parent::boot();

    static::creating(function($model) {
      return $model->validate();
    });

    static::updating(function($model) {
      return $model->validate();
    });

    static::deleting(function($model) {
      return $model->onDelete();
    });
  }

  public function hasValidationErrors() {
    return !empty($this->validationErrors);
  }

  public function getValidationMessages() {
    return $this->validationErrors;
  }

  public function getValidationMessage($name) {
    if (is_object($this->validationErrors))
      return $this->validationErrors->get($name);

    return null;
  }

  public function validate() {
    $validator = \Validator::make($this->attributes, $this->validationRules, $this->validationMessages);

    if ($validator->fails()) {
      $this->validationErrors = $validator->errors();
      return false;
    }

    return true;
  }

  public function onDelete() {
    return true;
  }

}
