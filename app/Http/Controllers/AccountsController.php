<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use \App\Models\User;

class AccountsController extends Controller {

  public function getActivate($token) {
    $user = User::whereActivationToken($token)->first();

    if (!$user)
      return view('accounts.message', [
        'type' => 'danger',
        'message' => trans('messages.error.activation_code_not_found')
        ]);

    if ($user->status === User::STATUS_ACTIVATED)
      return view('accounts.message', [
        'type' => 'info',
        'message' => trans('messages.info.account_was_activated')
      ]);

    if ($user->status === User::STATUS_NEW) {
      $user->status = User::STATUS_ACTIVATED;
      $user->save();
      
      \Auth::login($user);

      return \Redirect::to('/');
    }
  }

}
