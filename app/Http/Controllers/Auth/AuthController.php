<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Mail;

class AuthController extends Controller {
  /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
   */

use AuthenticatesAndRegistersUsers {
    postRegister as parentPostRegister;
  }

use ThrottlesLogins;

  protected $loginPath = 'login';
  protected $redirectPath = '/';

  /**
   * Create a new authentication controller instance.
   *
   * @return void
   */
  public function __construct() {
    $this->middleware('guest', ['except' => 'getLogout']);
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data) {
    return Validator::make($data, [
        'name'     => 'required|max:255',
        'email'    => 'required|email|max:255|unique:users',
        'password' => 'required|confirmed|min:6',
    ]);
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return User
   */
  protected function create(array $data) {
    return User::create([
        'name'             => $data['name'],
        'email'            => $data['email'],
        'password'         => bcrypt($data['password']),
        'activation_token' => bcrypt(str_random(50))
    ]);
  }

  public function postRegister(Request $request) {
    $request->merge([
      'password_confirmation' => $request->input('password'),
    ]);

    $result = $this->parentPostRegister($request);

    if ($result) {
      $user = \Auth::User();

      Mail::send('emails.auth.register', ['user' => $user], function ($m) use ($user) {
        $m->to($user->email, $user->name)->subject('Welcome to ' . config('app.project_name') . '!');
      });
    }
    
    \Auth::logout();

    return redirect('register-success');
  }
  
  public function getRegisterSuccess() {
    return view('auth.register-success', [
      'type' => 'info',
      'message' => trans('messages.success.account_was_created')
    ]);
  }

  protected function authenticated(Request $request, User $user) {
    switch ($user->status) {
      case User::STATUS_NEW:
        \Auth::logout();

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
              'status' => trans('messages.error.account_is_not_activated'),
        ]);
        
      case User::STATUS_BLOCKED:
        \Auth::logout();

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
              'status' => trans('messages.error.account_is_blocked'),
        ]);

      default:
        return redirect()->intended($this->redirectPath());
    }
  }

}
