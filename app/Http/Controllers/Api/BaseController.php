<?php

namespace App\Http\Controllers\Api;

/**
 * Description of BaseController
 *
 * @author Alex
 */
class BaseController extends \App\Http\Controllers\Controller {

  const RENDER_HTML = 'html';
  const RENDER_JSON = 'json';
  
  public function __construct() {
    $this->middleware('auth.basic');
  }

}
