<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AuthController extends \App\Http\Controllers\Controller {

  use AuthenticatesUsers;

  use ThrottlesLogins;

  /**
   * Create a new authentication controller instance.
   *
   * @return void
   */
  public function __construct() {
    $this->middleware('guest', ['except' => 'postLogout']);
  }

  protected function sendLockoutResponse(Request $request) {
    $seconds = app(RateLimiter::class)->availableIn(
      $request->input($this->loginUsername()) . $request->ip()
    );

    return response(json_encode([
      'message' => $this->getLockoutErrorMessage($seconds)
      ]), 401);
  }

  protected function authenticated(Request $request, User $user) {
    switch ($user->status) {
      case User::STATUS_NEW:
        \Auth::logout();

        return response(json_encode([
          'message' => trans('messages.error.account_is_blocked')
          ]), 401);

      case User::STATUS_BLOCKED:
        \Auth::logout();

        return response(json_encode([
          'message' => trans('messages.error.account_is_not_activated')
          ]), 403);

      default:
        return response(json_encode([
          'message' => 'ok',
          '_token'  => csrf_token(),
          'user'    => $user
        ]));
    }
  }

  protected function handleUserWasAuthenticated(Request $request, $throttles) {
    if ($throttles) {
      $this->clearLoginAttempts($request);
    }

    if (method_exists($this, 'authenticated')) {
      return $this->authenticated($request, \Auth::user());
    }

    return response(json_encode([
      'message' => 'ok',
      '_token'  => csrf_token(),
      'user'    => \Auth::user()
    ]));
  }

  /**
   * Handle a login request to the application.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function postLogin(Request $request) {
    $this->validate($request, [
      $this->loginUsername() => 'required',
      'password'             => 'required',
    ]);

    // If the class is using the ThrottlesLogins trait, we can automatically throttle
    // the login attempts for this application. We'll key this by the username and
    // the IP address of the client making these requests into this application.
    $throttles = $this->isUsingThrottlesLoginsTrait();

    if ($throttles && $this->hasTooManyLoginAttempts($request)) {
      return $this->sendLockoutResponse($request);
    }

    $credentials = $this->getCredentials($request);

    if (\Auth::attempt($credentials, $request->has('remember'))) {
      return $this->handleUserWasAuthenticated($request, $throttles);
    }

    // If the login attempt was unsuccessful we will increment the number of attempts
    // to login and redirect the user back to the login form. Of course, when this
    // user surpasses their maximum number of attempts they will get locked out.
    if ($throttles) {
      $this->incrementLoginAttempts($request);
    }

    return response(json_encode([
      'message' => $this->getFailedLoginMessage(),
      ]), 401);
  }

  /**
   * Log the user out of the application.
   *
   * @return \Illuminate\Http\Response
   */
  public function postLogout() {
    \Auth::logout();

    return response(json_encode(['message' => 'ok']));
  }

}
