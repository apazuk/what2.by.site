<?php

namespace App\Http\Controllers\Api\Lists;

use Illuminate\Http\Request;
//use Mail;

/**
 * Description of Items
 *
 * @author Alex
 */
class ItemsController extends \App\Http\Controllers\Api\BaseController {

//  protected function sendNotifications($recepients, $subject, $message) {
//    foreach ($recepients as $recepient) {
//      if ($recepient->user->id === \Auth::User()->id)
//        continue;
//
//      Mail::send('emails.api.lists.list_changed_notification', ['text' => $message], function ($m) use ($recepient, $subject) {
//        $m->to($recepient->user->email, $recepient->user->name)->subject($subject);
//      });
//    }
//  }

  private function renderItems($request, $list) {
    $result = null;

    $items = $list->items;

    switch ($request->get('render', self::RENDER_JSON)) {
      case self::RENDER_HTML:
        $result = \View::make('api.lists.items._content', ['list' => $list, 'items' => $items])->render();
        break;
      case self::RENDER_JSON:
        $result = $items;
        break;
    }

    return $result;
  }

  /**
   * Returns all items of requested checklist
   * 
   * @param Request $request
   * @return mix rendered list or error message
   */
  public function getIndex(Request $request) {
    $checklistId = $request->get('checklist_id');
    $list = \App\Models\Checklist::find($checklistId);

    if (!$list)
      return \Response::json(['message' => trans('messages.error.record_not_found', ['name' => 'List'])], 404);

    if (!$list->iHaveAccess())
      return \Response::json(['message' => trans('lists.api.list_unaccessd')], 403);

    return \Response::json([
        'checklist_id' => $checklistId,
        'items'        => $this->renderItems($request, $list),
    ]);
  }

  private function renderItem($request, $item) {
    $result = null;

    switch ($request->get('render', self::RENDER_JSON)) {
      case self::RENDER_HTML:
        $result = \View::make('api.lists.items._item', ['item' => $item])->render();
        break;
      case self::RENDER_JSON:
        $result = $item;
        break;
    }

    return $result;
  }

  /**
   * Returns a list item by id
   * @param Request $request
   * @param type $id
   * @return type
   */
  public function getView(Request $request, $id) {
    $item = \App\Models\ListItem::find($id);

    if (!$item)
      return \Response::json(['message' => trans('messages.error.record_not_found', ['name' => 'List item'])], 404);

    if (!$item->checklist->iHaveAccess())
      return \Response::json(['message' => trans('lists.api.list_unaccessd')], 403);

    return \Response::json([
        'id'           => $id,
        'checklist_id' => $item->checklist->id,
        'item'         => $this->renderItem($request, $item),
    ]);
  }

  public function postCreate(Request $request) {
    $item = new \App\Models\ListItem($request->input());
    $item->state = \App\Models\ListItem::STATE_OPEN;

    if ($item->validate()) {
      if (!$item->save())
        return \Response::json(['message' => trans('messages.error.something_wrong')], 500);
    }

    if ($item->hasValidationErrors())
      return \Response::json($item->getValidationMessages(), 401);

//    $list = $item->checklist;

//    $this->sendNotifications(
//      $list->sharedTo, trans('lists.api.list_changed', ['name' => $list->name]), trans('lists.api.item_added_to_list', ['user' => \Auth::User()->name, 'list' => $list->name, 'item' => $item->name])
//    );
//
//    $this->sendNotifications(
//      [$list], trans('lists.api.list_changed', ['name' => $list->name]), trans('lists.api.item_added_to_list', ['user' => \Auth::User()->name, 'list' => $list->name, 'item' => $item->name])
//    );

    return \Response::json([
        'id'           => $item->id,
        'checklist_id' => $item->checklist->id,
        'class'        => 'list.item',
        'message'      => trans('messages.success.added', ['name' => 'Item']),
        'item'         => $this->renderItem($request, $item),
    ]);
  }

  public function postCheck(Request $request, $id) {
    $item = \App\Models\ListItem::find($id);

    if (!$item)
      return \Response::json(['message' => trans('messages.error.record_not_found', ['name' => 'Item'])], 404);
    
    if (!$item->checklist->iHaveAccess())
      return \Response::json(['message' => trans('lists.api.list_unaccessd')], 403);

    switch ($item->state) {
      case \App\Models\ListItem::STATE_CLOSED:
        $item->state = \App\Models\ListItem::STATE_OPEN;
        break;
      default:
        $item->state = \App\Models\ListItem::STATE_CLOSED;
    }

    if ($item->validate())
      $item->save();

    if ($item->hasValidationErrors())
      return \Response::json($item->getValidationMessages(), 401);

//    if ($item->state === \App\Models\ListItem::STATE_OPEN) {
//      $list = $item->checklist;
//
//      $this->sendNotifications(
//        $list->sharedTo, trans('lists.api.list_changed', ['name' => $list->name]), trans('lists.api.item_changed_to_open', ['user' => \Auth::User()->name, 'list' => $list->name, 'item' => $item->name])
//      );
//
//      $this->sendNotifications(
//        [$list], trans('lists.api.list_changed', ['name' => $list->name]), trans('lists.api.item_changed_to_open', ['user' => \Auth::User()->name, 'list' => $list->name, 'item' => $item->name])
//      );
//    }

    return \Response::json([
        'id'      => $id,
        'message' => trans('messages.success.updated', ['name' => 'Item']),
        'state'   => $item->state,
        'item'    => $this->renderItem($request, $item),
        ]);
  }

  public function postUpdate(Request $request, $id) {
    $item = \App\Models\ListItem::find($id);

    if (!$item)
      return \Response::json(['message' => trans('messages.error.record_not_found', ['name' => 'Item'])], 404);
    
    if (!$item->checklist->iHaveAccess())
      return \Response::json(['message' => trans('lists.api.list_unaccessd')], 403);

    $item->fill($request->input());

    if ($item->validate())
      $item->save();

    if ($item->hasValidationErrors())
      return \Response::json($item->getValidationMessages(), 401);

    return \Response::json([
        'id'      => $id,
        'class'   => 'list.item',
        'message' => trans('messages.success.updated', ['name' => 'Item']),
        'item'    => $this->renderItem($request, $item),
        ]);
  }

  public function postDelete($id) {
    $item = \App\Models\ListItem::find($id);

    if (!$item)
      return \Response::json(['message' => trans('messages.error.record_not_found', ['name' => 'Item'])], 404);
    
    if (!$item->checklist->iHaveAccess())
      return \Response::json(['message' => trans('lists.api.list_unaccessd')], 403);

    if ($item->delete())
      return \Response::json([
          'id'      => $id,
          'class'   => 'list.item',
          'message' => trans('messages.success.deleted', ['name' => 'Item']),
          ], 200);

    return \Response::json(['message' => trans('messages.error.something_wrong')], 500);
  }

}
