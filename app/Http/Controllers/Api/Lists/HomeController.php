<?php

namespace App\Http\Controllers\Api\Lists;

use Illuminate\Http\Request;

/**
 * Description of HomeController
 *
 * @author Alex
 */
class HomeController extends \App\Http\Controllers\Api\BaseController {

  /**
   * Returns list of all user's lists
   * @param Request $request
   */
  public function getIndex() {
    return \Response::json([
        'my'     => \App\Models\Checklist::my()->get(),
        'shared' => \App\Models\Checklist::shared()->select('checklist_id as id', 'name')->get()
    ]);
  }

  private function renderList($request, $list) {
    $result = null;

    switch ($request->get('render', self::RENDER_JSON)) {
      case self::RENDER_HTML:
        $result = \View::make('api.lists.home._list', ['list' => $list])->render();
        break;
      case self::RENDER_JSON:
        // fetch all items
        $list->items;
        $result = $list;
        break;
    }

    return $result;
  }

  public function getView(Request $request, $id) {
    $list = \App\Models\Checklist::find($id);

    if (!$list)
      return \Response::json(['message' => trans('messages.error.record_not_found', ['name' => 'List'])], 404);

    if (!$list->iHaveAccess())
      return \Response::json(['message' => trans('lists.api.list_unaccessd')], 403);

    return \Response::json([
        'id'     => $id,
        'result' => $this->renderList($request, $list),
    ]);
  }

  public function postCreate(Request $request) {
    $request->merge([
      'user_id' => \Auth::User()->id
    ]);
    $list = \App\Models\Checklist::create($request->input());

    if (!$list)
      return \Response::json(['message' => trans('messages.error.something_wrong')], 500);

    if ($list->validate())
      $list->save();

    if ($list->hasValidationErrors())
      return \Response::json($list->getValidationMessages(), 401);

    return \Response::json([
        'id'      => $list->id,
        'class'   => 'list',
        'message' => trans('messages.success.added', ['name' => 'List']),
        'result'  => $this->renderList($request, $list),
        ], 200);
  }

  public function postUpdate(Request $request, $id) {
    $list = \App\Models\Checklist::find($id);

    if (!$list)
      return \Response::json(['message' => trans('messages.error.record_not_found', ['name' => 'List'])], 404);

    $list->fill($request->input());

    if ($list->validate())
      $list->save();

    if ($list->hasValidationErrors())
      return \Response::json($list->getValidationMessages(), 401);

    return \Response::json([
        'id'      => $list->id,
        'class'   => 'list',
        'message' => trans('messages.success.updated', ['name' => 'List']),
        'result'  => $this->renderList($request, $list),
        ], 200);
  }

  public function postDelete($id) {
    $list = \App\Models\Checklist::find($id);

    if (!$list)
      return \Response::json(['message' => trans('messages.error.record_not_found', ['name' => 'List'])], 404);

    if ($list->delete())
      return \Response::json([
          'id'      => $id,
          'class'   => 'list',
          'message' => trans('messages.success.deleted', ['name' => 'List']),
          ], 200);

    return \Response::json(['message' => trans('messages.error.something_wrong')], 500);
  }

  public function postShare(Request $request, $id) {
    $list = \App\Models\Checklist::find($id);

    if (!$list)
      return \Response::json(['message' => trans('messages.error.record_not_found', ['name' => 'List'])], 404);

    $emails = explode(',', $request->input('people'));

    $succeeded = 0;

    foreach ($emails as $email) {
      $email = trim($email);

      $user = \App\Models\User::whereEmail($email)->first();

      if ($user) {
        $record = \App\Models\ListShare::create([
            'checklist_id' => $list->id,
            'user_id'      => $user->id
        ]);

        //  TODO: send notification

        if ($record)
          $succeeded++;
      } else {
        //  TODO: send invitation
      }
    }

    if ($succeeded > 0)
      return \Response::json([
          'id'      => $list->id,
          'class'   => 'list',
          'message' => trans('messages.success.shared', ['name' => 'List']),
          ], 200);

    return \Response::json(['message' => trans('messages.error.something_wrong')], 500);
  }

}
