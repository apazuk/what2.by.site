<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Traits\DbDebug;

abstract class Controller extends BaseController
{

	use DispatchesJobs,
	 ValidatesRequests,
	 DbDebug;
}
