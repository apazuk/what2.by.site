<?php

namespace App\Http\Controllers;

use \App\Models\Checklist;

class HomeController extends BaseController
{

	public function getIndex()
	{
		if (\Auth::check())
			return $this->getLists();

		return $this->getLanding();
	}

	private function getLanding()
	{
		return view('home.index');
	}

	private function getLists()
	{
		$listsMy = Checklist::my()
			->with(['items'])
			->get();
			
		$listsShared = Checklist::shared()->get();

		return view('home.lists', [
			'listsMy' => $listsMy,
			'listsShared' => $listsShared,
		]);
	}

}
