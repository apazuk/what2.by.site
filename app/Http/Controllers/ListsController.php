<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Checklist;

class ListsController extends BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$lists = Checklist::my()->get();

		return view('lists.index', [
			'lists' => $lists
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getView($id)
	{
		$list = Checklist::findOrFail($id);

		return view('lists.view', [
			'list' => $list
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return view('lists.create', [
			'list' => new Checklist(\Input::old())
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function postCreate(Request $request)
	{
		$list = Checklist::create([
				'user_id' => \Auth::User()->id,
				'name' => $request->input('name')
		]);

		if (!$list)
			return \Redirect::to('lists')
					->with('error-message', trans('messages.error.something_wrong'));

		if ($list->hasValidationErrors())
			return \Redirect::to('lists/create')
					->withInput($request->input())
					->withErrors($list->getValidationMessages());

		return \Redirect::to('lists')
				->with('success-message', trans('messages.success.added', ['name' => 'List']));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getUpdate($id)
	{
		$list = Checklist::find($id);

		if (!$list)
			\App::abort(404, trans('messages.error.record_not_found', ['List']));

		$list->fill(\Input::old());

		return \View::make('lists.update', array(
				'list' => $list,
		));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Request  $request
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate(Request $request, $id)
	{
		$list = Checklist::find($id);

		if (!$list)
			App::abort(404, trans('messages.error.record_not_found', ['List']));

		$list->fill($request->input());

		if ($list->validate())
			$list->save();

		if ($list->hasValidationErrors())
			return \Redirect::to(url('lists/update', ['id' => $id]))
					->withInput($request->input())
					->withErrors($list->getValidationMessages());

		return \Redirect::to('lists')
				->with('success-message', trans('messages.success.updated', ['name' => 'List']));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postDelete($id)
	{
		$list = Checklist::find($id);

		if (!$list)
			\App::abort(404, trans('messages.error.record_not_found', ['List']));

		if ($list->delete())
			return \Redirect::to('lists')
					->with('success-message', trans('messages.success.deleted', ['name' => 'List']));
		else
			return \Redirect::to('lists')
					->with('error-message', trans('messages.error.something_wrong'));
	}

//	public function getTest()
//	{
//		$list = Checklist::find(22);
//
//		$items = \App\Models\ListItem::where('checklist_id', '=', $list->id)
//			->orderBy('state')
//			->orderBy('name')
//			->get();
//
//		foreach ($items as $item) {
////			var_dump([
////				$item['attributes'],
////				strtotime($item->updated_at),
////				strtotime('- 10 minutes'),
////				strtotime($item->updated_at) > strtotime('- 10 minutes')
////			]);
//
//			switch ($item->state) {
//				case \App\Models\ListItem::STATE_OPEN:
//					if (strtotime($item->updated_at) > strtotime('- 10 minutes'))
//						echo "<p style=\"color: green;\">{$item->name}</p>";
//					else
//						echo "<p style=\"color: black;\">{$item->name}</p>";
//					break;
//				case \App\Models\ListItem::STATE_CLOSED:
//					if (strtotime($item->updated_at) > strtotime('- 10 minutes'))
//						echo "<p style=\"color: red;\">{$item->name}</p>";
////					else
////						echo "<p style=\"color: gray;\">{$item->name}</p>";
//					break;
//			}
//		}
//	}

}
