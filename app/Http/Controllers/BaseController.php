<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 * Description of BaseController
 *
 * @author Alex
 */
class BaseController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');

		if (\App::environment('local'))
			$this->enableLoggin();
	}

}
