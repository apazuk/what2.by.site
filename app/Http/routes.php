<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@getIndex');

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@postRegister');
Route::get('register-success', 'Auth\AuthController@getRegisterSuccess');

Route::controller('home', 'HomeController');
Route::controller('account', 'AccountsController');

Route::get('lists/view/{id}', 'ListsController@getView');

//Route::get('lists/test', 'ListsController@getTest');

Route::group(['prefix' => 'api'], function () {
  Route::controller('auth', 'Api\Auth\AuthController');
  
  Route::get('lists/{id}/items', 'Api\Lists\ItemsController@getView');
  
  Route::controller('lists/items', 'Api\Lists\ItemsController');
  Route::controller('lists', 'Api\Lists\HomeController');
});
