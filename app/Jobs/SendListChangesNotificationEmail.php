<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Models\Checklist;

class SendListChangesNotificationEmail extends Job implements SelfHandling, ShouldQueue
{

	use InteractsWithQueue,
	 SerializesModels;

	protected $checklist;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct(Checklist $checklist)
	{
		$this->checklist = $checklist;
	}

	protected function sendNotifications($recepients, $items)
	{
		foreach ($recepients as $recepient) {
//			if ($recepient->user->id === \Auth::User()->id)
//				continue;

			Mail::send('emails.lists.list_changed_notification', [
				'list' => $this->checklist,
				'items' => $items
			], function ($m) use ($recepient) {
				$m
					->to($recepient->user->email, $recepient->user->name)
					->subject(trans('lists.api.list_changed', ['name' => $this->checklist->name]));
			});
		}
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$items = \App\Models\ListItem::where('checklist_id', '=', $this->checklist->id)
			->orderBy('state')
			->orderBy('name')
			->get();

		$this->sendNotifications(
			$this->checklist->sharedTo, $items
		);

		$this->sendNotifications(
			[$this->checklist], $items
		);
	}

}
